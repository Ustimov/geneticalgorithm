__author__ = 'Artem Ustimov'

import unittest
import tsp
import random


class Test(unittest.TestCase):
    def test_mutate(self):
        count = random.randint(0, 100)
        original_individ = [tsp.City(i, i, i) for i in range(count)]
        mutated_individ = tsp.mutate(original_individ)
        for i in range(count):
            if original_individ[i] != mutated_individ[i]:
                j = original_individ.index(mutated_individ[i])
                self.assertNotEqual(original_individ[j], mutated_individ[j])
                self.assertEqual(original_individ[j], mutated_individ[i])

    def test_path(self):
        self.assertEqual(tsp.path(tsp.City(0, 0, 0), tsp.City(1, 1, 1)), 2 ** (1 / 2))
        self.assertEqual(tsp.path(tsp.City(4, 4, 4), tsp.City(1, 1, 1)), 18 ** (1 / 2))
        self.assertEqual(tsp.path(tsp.City(1, 2, 3), tsp.City(4, 5, 6)), 18 ** (1 / 2))

    def test_crossover(self):
        count = random.randint(0, 100)
        original_individ = [tsp.City(i, i, i) for i in range(count)]
        individ1, individ2 = original_individ[1:], original_individ[1:]
        random.shuffle(individ1)
        random.shuffle(individ2)
        individ1, individ2 = [original_individ[0]] + individ1, [original_individ[0]] + individ2
        individ3 = tsp.crossover(individ1, individ2)
        individ3.sort(key=lambda item: item.name)
        self.assertEqual(len(individ3), count)
        self.assertListEqual(original_individ, individ3)

    def test_path_len(self):
        city1, city2, city3, city4 = tsp.City(0, 0, 0), tsp.City(1, 1, 1), tsp.City(2, 2, 2), tsp.City(2, 2, 0)
        self.assertEqual(tsp.calc_path([city1, city2, city3, city4]), 8 ** (1 / 2) + 2)
        self.assertEqual(tsp.calc_path([city1, city3, city4, city2]), 8 ** (1 / 2) + 2 + 2 ** (1 / 2))

    def test_fitness(self):
        count = random.randint(0, 100)
        original_individ = [tsp.City(i, i * i, i) for i in range(count)]
        individs = [original_individ[:] for i in range(count)]
        for individ in individs:
            random.shuffle(individ)
        paths = [tsp.calc_path(individ) for individ in individs]
        m = min(paths)
        best_individ, best_path = tsp.fitness(individs)
        self.assertEqual(m, best_path)
        self.assertEqual(individs[paths.index(m)], best_individ)

    def test_select(self):
        count = random.randint(0, 100)
        original_individ = [tsp.City(i, i, i) for i in range(count)]
        population1 = [original_individ[:] for i in range(count)]
        self.assertEqual(len(tsp.select(population1, int(count / 2))), int(count / 2))
        city1, city2, city3, city4 = tsp.City(0, 0, 0), tsp.City(1, 1, 1), tsp.City(2, 2, 2), tsp.City(2, 2, 0)
        population2 = [[city1, city2, city3, city4], [city1, city3, city4, city2]]
        result = tsp.select(population2, 1)
        self.assertEqual(len(result), 1)
        self.assertEqual(population2[0], result[0])


if __name__ == "__main__":
    unittest.main()