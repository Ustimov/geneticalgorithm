__author__ = 'Artem Ustimov'

import sys
from random import randint, shuffle


class City:
    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Name: {0} | Coords: ({1}, {2})'.format(self.name, self.x, self.y)


def mutate(individ):
    individ = individ[:]
    max_value = len(individ) - 1
    i, j = randint(0, max_value), randint(0, max_value)
    individ[i], individ[j] = individ[j], individ[i]
    return individ


def path(city1, city2):
    return ((city1.x - city2.x) ** 2 + (city1.y - city2.y) ** 2) ** (1 / 2)


def crossover(individ1, individ2):
    individ1, individ2 = individ1[:], individ2[:]
    new_individ = []
    new_individ.append(individ1[0])
    individ1.remove(new_individ[-1])
    individ2.remove(new_individ[-1])
    while individ1:
        if path(new_individ[0], individ1[0]) <= path(new_individ[0], individ2[0]):
            new_individ.append(individ1[0])
        else:
            new_individ.append(individ2[0])
        individ1.remove(new_individ[-1])
        individ2.remove(new_individ[-1])
    return new_individ


def calc_path(individ):
    path_len = 0
    for i in range(len(individ) - 1):
        path_len += path(individ[i], individ[i + 1])
    return path_len


def fitness(population):
    best_individ = population[0]
    best_path_len = calc_path(best_individ)
    for individ in population:
        if best_path_len > calc_path(individ):
            best_individ = individ
            best_path_len = calc_path(best_individ)
    return best_individ, best_path_len


def select(new_generation, count):
    index = []
    for individ in new_generation:
        index.append([new_generation.index(individ), calc_path(individ)])
    index.sort(key=lambda item: item[1])
    return [new_generation[i[0]] for i in index[:count]]


def generate(population):
    new_generation = []
    for i in range(len(population)):
        for j in range(i, len(population)):
            individ = crossover(population[i], population[j])
            individ = mutate(individ)
            new_generation.append(individ)
    return select(new_generation, len(population))

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Three args are required to start program:\n'
              '1. Path to input file\n'
              '2. Size of population\n'
              '3. Number of generations')
        exit()
    individ = []
    for line in open(sys.argv[1]):
        name, x, y = line.split('\t')
        x, y = int(x), int(y)
        individ.append(City(name, x, y))
    population = [individ[1:] for i in range(int(sys.argv[2]))]
    for i in population:
        shuffle(i)
    population = [[individ[0]] + i for i in population]
    best_individ, best_path = fitness(population)
    for i in range(int(sys.argv[3])):
        population = generate(population)
        new_best_individ, new_best_path = fitness(population)
        if best_path > new_best_path:
            best_individ = new_best_individ
            best_path = new_best_path
    for city in best_individ:
        if city != best_individ[-1]:
            print('{0}->'.format(city.name), end='')
        else:
            print(city.name)
    print('Total path: {0}'.format(best_path))